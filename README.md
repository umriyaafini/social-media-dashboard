# About
*KumSocial* is a Social Media Dashboard that has 3 main features Users, Posts, and Albums.

## Feature

Feature in this app is:

- User can view list of users
- User can view list of posts of each user
- User can view list of albums of each user
- User can view the detail of each post and its comment
- User can view list of photos from an album
- User can view the detail of photo
- User can add, edit and delete post
- User can add, edit and delete comment

# Development
This project is bootstrapped with [Create React App](https://github.com/facebook/create-react-app), the UI components is from [reactstrap](https://reactstrap.github.io/) and [Bootstrap](https://getbootstrap.com/) and
Redux is used to manage Network API and global state.

This project is hosted on [Netlify](https://www.netlify.com/) for demo please kindly check [Social Media Dashboard](https://nifty-hodgkin-6107e5.netlify.com).

## Running On Local Machine

In the project directory, you can run:

### `npm install`

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
PS: Currently the unit test only cover components.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

Thank you ❤️