import * as ActionType from '../action/actionType';

const initialState = {
  isFetchingUsers: false,
  isFetchingUserDetail: false,
  isFetchingUserAlbums: false,
  isFetchingUserPosts: false,
  all: [],
  details: [],
  albums: [],
  posts: [],
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.FETCH_ALL_USER_REQUEST:
      return {
        ...state,
        isFetchingUsers: true,
      };
    case ActionType.FETCH_ALL_USER_SUCCESS:
      return {
        ...state,
        isFetchingUsers: false,
        all: action.payload,
      };
    case ActionType.FETCH_ALL_USER_FAILURE:
      return {
        ...state,
        isFetchingUsers: false,
      };
    case ActionType.FETCH_USER_DETAIL_REQUEST:
      return {
        ...state,
        isFetchingUserDetail: true,
      };
    case ActionType.FETCH_USER_DETAIL_SUCCESS:
      return {
        ...state,
        details: {
          ...state.details,
          [action.payload.id]: action.payload,
        },
        isFetchingUserDetail: false,
      };
    case ActionType.FETCH_USER_DETAIL_FAILURE:
      return {
        ...state,
        isFetchingUserDetail: false,
      };
    case ActionType.FETCH_USER_ALBUMS_REQUEST:
      return {
        ...state,
        isFetchingUserAlbums: true,
      };
    case ActionType.FETCH_USER_ALBUMS_SUCCESS:
      return {
        ...state,
        isFetchingUserAlbums: false,
        albums: action.payload,
      };
    case ActionType.FETCH_USER_ALBUMS_FAILURE:
      return {
        ...state,
        isFetchingUserAlbums: false,
      };
    case ActionType.FETCH_USER_POSTS_REQUEST:
      return {
        ...state,
        isFetchingUserPosts: true,
      };
    case ActionType.FETCH_USER_POSTS_SUCCESS:
      return {
        ...state,
        isFetchingUserPosts: false,
        posts: action.payload,
      };
    case ActionType.FETCH_USER_POSTS_FAILURE:
      return {
        ...state,
        isFetchingUserPosts: false,
      };
    default:
      return state;
  }
};

export default user;
