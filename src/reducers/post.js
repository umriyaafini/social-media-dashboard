import * as ActionType from '../action/actionType';

const initialState = {
  isFetchingPosts: false,
  isFetchingPostDetail: false,
  isAddingNewPost: false,
  isUpdatingPost: false,
  isDeletingPost: false,
  all: [],
  details: {},
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.FETCH_ALL_POST_REQUEST:
      return {
        ...state,
        isFetchingPosts: true,
      };
    case ActionType.FETCH_ALL_POST_SUCCESS:
      return {
        ...state,
        isFetchingPosts: false,
        all: action.payload,
      };
    case ActionType.FETCH_ALL_POST_FAILURE:
      return {
        ...state,
        isFetchingPosts: false,
      };
    case ActionType.FETCH_POST_DETAIL_REQUEST:
      return {
        ...state,
        isFetchingPostDetail: true,
      };
    case ActionType.FETCH_POST_DETAIL_SUCCESS:
      return {
        ...state,
        details: {
          ...state.details,
          [action.payload.id]: action.payload,
        },
        isFetchingPostDetail: false,
      };
    case ActionType.FETCH_POST_DETAIL_FAILURE:
      return {
        ...state,
        isFetchingPostDetail: false,
      };
    case ActionType.ADD_NEW_POST_REQUEST:
      return {
        ...state,
        isAddingNewPost: true,
      };
    case ActionType.ADD_NEW_POST_SUCCESS:
      return {
        ...state,
        details: {
          ...state.details,
          [action.payload.id]: action.payload,
        },
        isAddingNewPost: false,
      };
    case ActionType.ADD_NEW_POST_FAILURE:
      return {
        ...state,
        isAddingNewPost: false,
      };
    case ActionType.EDIT_POST_REQUEST:
      return {
        ...state,
        isUpdatingPost: true,
      };
    case ActionType.EDIT_POST_SUCCESS:
      return {
        ...state,
        details: {
          ...state.details,
          [action.payload.id]: action.payload,
        },
        isUpdatingPost: false,
      };
    case ActionType.EDIT_POST_FAILURE:
      return {
        ...state,
        isUpdatingPost: false,
      };
    case ActionType.DELETE_POST_REQUEST:
      return {
        ...state,
        isDeletingPost: true,
      };
    case ActionType.DELETE_POST_SUCCESS:
    case ActionType.DELETE_POST_FAILURE:
      return {
        ...state,
        isDeletingPost: false,
      };
    default:
      return state;
  }
};

export default user;
