import { combineReducers } from 'redux';

import album from './album';
import comment from './comment';
import post from './post';
import snackbar from './snackbar';
import user from './user';

const appReducer = combineReducers({
  album,
  comment,
  post,
  snackbar,
  user,
});

export default appReducer;