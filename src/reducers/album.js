import * as ActionType from '../action/actionType';

const initialState = {
  isFetchingAlbums: false,
  isFetchingAlbumDetail: false,
  isFetchingPhotos: false,
  details: {},
  photos: [],
  all: [],
};

const album = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.FETCH_ALL_ALBUM_REQUEST:
      return {
        ...state,
        isFetchingAlbums: true,
      };
    case ActionType.FETCH_ALL_ALBUM_SUCCESS:
      return {
        ...state,
        isFetchingAlbums: false,
        all: action.payload,
      };
    case ActionType.FETCH_ALL_ALBUM_FAILURE:
      return {
        ...state,
        isFetchingAlbums: false,
      };
    case ActionType.FETCH_ALBUM_DETAIL_REQUEST:
      return {
        ...state,
        isFetchingAlbumDetail: true,
      };
    case ActionType.FETCH_ALBUM_DETAIL_SUCCESS:
      return {
        ...state,
        isFetchingAlbumDetail: false,
        details: {
          ...state.details,
          [action.payload.id]: action.payload,
        },
      };
    case ActionType.FETCH_ALBUM_DETAIL_FAILURE:
      return {
        ...state,
        isFetchingAlbumDetail: false,
      };
    case ActionType.FETCH_ALL_PHOTO_REQUEST:
      return {
        ...state,
        isFetchingPhotos: true,
      };
    case ActionType.FETCH_ALL_PHOTO_SUCCESS:
      return {
        ...state,
        photos: action.payload,
        isFetchingPhotos: false,
      };
    case ActionType.FETCH_ALL_PHOTO_FAILURE:
      return {
        ...state,
        isFetchingPhotos: false,
      };
    default:
      return state;
  }
};

export default album;
