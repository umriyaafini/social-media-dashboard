import * as actionType from '../action/actionType';

const initialState = {
  isShowSnackBar: false,
  message: '',
  type: 'success'
};

const snackbar = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SHOW_SNACKBAR:
      return {
        ...state,
        isShowSnackBar: true,
        message: action.payload.message,
      };
    case actionType.HIDE_SNACKBAR:
      return {
        ...state,
        isShowSnackBar: false,
        message: '',
      };
    default:
      return state;
  }
};

export default snackbar;
