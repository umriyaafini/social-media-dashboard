import * as ActionType from '../action/actionType';

const initialState = {
  isFetchingComments: false,
  isAddingNewComment: false,
  isUpdatingComment: false,
  isDeletingComment: false,
  all: [],
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.FETCH_ALL_COMMENT_REQUEST:
      return {
        ...state,
        isFetchingComments: true,
      };
    case ActionType.FETCH_ALL_COMMENT_SUCCESS:
      return {
        ...state,
        isFetchingComments: false,
        all: action.payload,
      };
    case ActionType.FETCH_ALL_COMMENT_FAILURE:
      return {
        ...state,
        isFetchingComments: false,
      };
    case ActionType.ADD_NEW_COMMENT_REQUEST:
      return {
        ...state,
        isAddingNewComment: true,
      };
    case ActionType.ADD_NEW_COMMENT_SUCCESS:
    case ActionType.ADD_NEW_COMMENT_FAILURE:
      return {
        ...state,
        isAddingNewComment: false,
      };
    case ActionType.EDIT_COMMENT_REQUEST:
      return {
        ...state,
        isUpdatingComment: true,
      };
    case ActionType.EDIT_COMMENT_SUCCESS:
    case ActionType.EDIT_COMMENT_FAILURE:
      return {
        ...state,
        isUpdatingComment: false,
      };
    case ActionType.DELETE_COMMENT_REQUEST:
      return {
        ...state,
        isDeletingComment: true,
      };
    case ActionType.DELETE_COMMENT_SUCCESS:
    case ActionType.DELETE_COMMENT_FAILURE:
      return {
        ...state,
        isDeletingComment: false,
      };
    default:
      return state;
  }
};

export default user;
