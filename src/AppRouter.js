import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from 'react-redux'
import configureStore from './store/configureStore';

import Layout from './components/Layout';

import Album from './containers/Album';
import AlbumDetail from './containers/AlbumDetail';
import Post from './containers/Post';
import PostDetail from './containers/PostDetail';
import User from './containers/User';
import UserPost from './containers/UserPost';
import UserAlbum from './containers/UserAlbum';

const store = configureStore();

function AppRouter() {
  return (
    <Router>
      <Provider store={store}>
        <Layout>
          <Switch>
            <Route path="/" exact component={User} />
            <Route path="/user/:userId/post" component={UserPost} />
            <Route path="/user/:userId/album" component={UserAlbum} />

            <Route path="/post/:userId/:postId" component={PostDetail} />
            <Route path="/post" component={Post} />

            <Route path="/album/:userId/:albumId" component={AlbumDetail} />
            <Route path="/album" component={Album} />
          </Switch>
        </Layout>
      </Provider>
    </Router>
  );
}

export default AppRouter;