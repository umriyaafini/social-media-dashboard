import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

const PhotoDetail = ({
  isOpen,
  url,
  title,
  toggle,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      toggle={toggle}
      size="lg"
    >
      <ModalHeader toggle={toggle}>Picture Detail</ModalHeader>
      <ModalBody>
        <img width="100%" src={url} alt="" />
        <h6>{title}</h6>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={toggle}>Close</Button>
      </ModalFooter>
    </Modal>
  );
}

PhotoDetail.propTypes = {
  isOpen: PropTypes.bool,
  toggle: PropTypes.func,
  title: PropTypes.string,
};

export default PhotoDetail;