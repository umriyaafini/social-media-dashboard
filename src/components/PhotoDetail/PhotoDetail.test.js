import React from "react";
import { shallow } from 'enzyme';
import PhotoDetail from './index';

const createTestProps = (props) => ({
  isOpen: true,
  toggle: jest.fn(),
  title: 'delectus sint molestias maiores et cupiditate',
  ...props,
});

describe('PhotoDetail component', () => {
  const createWrapper = props => shallow(<PhotoDetail {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('when given props url', () => {
      const props  = createTestProps({ url: 'https://via.placeholder.com/600/1567c' });
      const wrapper = createWrapper(props);
      it('should render the photo', () => {
        expect(wrapper.find('img')).toHaveLength(1);
      })
    });

  });
});