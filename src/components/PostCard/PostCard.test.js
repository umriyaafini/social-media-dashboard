import React from "react";
import { shallow } from 'enzyme';
import PostCard from './index';

const createTestProps = (props) => ({
  name: 'Nikola Tesla',
  title: 'Electrical Engineering',
  body: 'My brain is only a receiver, in the Universe there is ...',
  ...props,
});

describe('PostCard component', () => {
  const createWrapper = props => shallow(<PostCard {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('when there is a props name', () => {
      it('should render <Avatar/>', () => {
        const props  = createTestProps()
        const wrapper = createWrapper(props);
        expect(wrapper.find('Avatar').exists()).toEqual(true);
      })
    });
  });
});