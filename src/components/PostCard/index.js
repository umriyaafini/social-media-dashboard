import React from 'react';
import PropTypes from 'prop-types';

import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
  ButtonGroup,
} from 'reactstrap';
import Avatar from '../Avatar';

const PostCard = ({
  name,
  title,
  body,
  onAddComment,
  onEdit,
  onDelete,
  onGoToDetail,
}) => {
  return (
    <Card className="post-card">
      <CardBody>
        {name && <Avatar name={name} />}
        <div>
          {name && <CardTitle tag="h5">{name}</CardTitle>}
          <CardTitle tag="h3">{title}</CardTitle>
          <CardText>{body}</CardText>
          {onGoToDetail
            && <Button className="mt-3" color="primary" onClick={onGoToDetail}>See details</Button>
          }
          {onAddComment
            && <Button className="mt-3" color="primary" onClick={onAddComment}>Add comment</Button>
          }
          {(onEdit || onDelete) && (
            <div className="floating-action">
              <ButtonGroup>
                <Button onClick={onEdit}>Edit</Button>
                <Button onClick={onDelete}>Delete</Button>
              </ButtonGroup>
            </div>
          )}
        </div>
      </CardBody>
    </Card>
  );
};

PostCard.propTypes = {
  body: PropTypes.string.isRequired,
  name: PropTypes.string,
  onAddComment:  PropTypes.func,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onGoToDetail: PropTypes.func,
  title: PropTypes.string.isRequired,
};

export default PostCard;