import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

import {
  ListGroup,
  ListGroupItem,
} from 'reactstrap';


const AlbumLists = ({ albums }) => {
  return (
    <ListGroup>
      {albums && albums.length > 0 && albums.map(album => (
        <ListGroupItem
          key={album.id}
          tag={Link}
          to={`/album/${album.userId}/${album.id}`}
        >
          <span role="img" aria-label="album">🗂</span>
          {album.title}
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};


AlbumLists.propTypes = {
  albums: PropTypes.array.isRequired
};

export default AlbumLists;
