import React from "react";
import { shallow } from 'enzyme';
import AlbumLists from './index';

const createTestProps = (props) => ({
  albums: [],
  ...props,
});

const albums = [
  {
    userId: 1,
    id: 1,
    title: 'quidem molestiae enim'
  },
  {
    userId: 1,
    id: 2,
    title: 'sunt qui excepturi placeat culpa'
  },
  {
    userId: 1,
    id: 3,
    title: 'omnis laborum odio'
  }
];

describe('AlbumLists component', () => {
  const createWrapper = props => shallow(<AlbumLists {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {
    describe('when there is a props albums', () => {
      it('should render list of albums', () => {
        const props  = createTestProps({ albums })
        const wrapper = createWrapper(props);
        expect(wrapper.find('ListGroupItem')).toHaveLength(3);
      })
    });

  });
});