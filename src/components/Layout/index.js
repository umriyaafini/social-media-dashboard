import React from 'react';
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

import {
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import SnackBar from '../SnackBar';

const Layout = ({ children }) => {
  return (
    <Container>
      <Row>
        <Col xs="12" md="3" lg="2" className="sidebar">
          <h3 className="app-name">KumSocial</h3>
          <Nav vertical>
            <NavItem>
              <NavLink tag={Link} to="/">Users</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/post">Posts</NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to="/album">Albums</NavLink>
            </NavItem>
          </Nav>
        </Col>

        <Col xs="12" md="9" lg="10" className="main-content">
          {children}
        </Col>
      </Row>
      <SnackBar />
    </Container>
  );
};


Layout.propTypes = {
  children: PropTypes.any,
};

export default Layout;
