import React from "react";
import { shallow } from 'enzyme';
import AlbumCard from './index';

const createTestProps = (props) => ({
  title: 'With Friend in Budapest',
  ...props,
});

const photos = [
  {
    albumId: 1,
    id: 1,
    title: 'accusamus beatae ad facilis cum similique qui sunt',
    url: 'https://via.placeholder.com/600/92c952',
    thumbnailUrl: 'https://via.placeholder.com/150/92c952'
  },
  {
    albumId: 1,
    id: 2,
    title: 'reprehenderit est deserunt velit ipsam',
    url: 'https://via.placeholder.com/600/771796',
    thumbnailUrl: 'https://via.placeholder.com/150/771796'
  },
  {
    albumId: 1,
    id: 3,
    title: 'officia porro iure quia iusto qui ipsa ut modi',
    url: 'https://via.placeholder.com/600/24f355',
    thumbnailUrl: 'https://via.placeholder.com/150/24f355'
  }
];

describe('AlbumCard component', () => {
  const createWrapper = props => shallow(<AlbumCard {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('when there is a props name', () => {
      it('should render <Avatar/>', () => {
        const props  = createTestProps({ name: 'Nikola Tesla'})
        const wrapper = createWrapper(props);
        expect(wrapper.find('Avatar').exists()).toEqual(true);
      })
    });

    describe('when there is no a props name', () => {
      it('should not render <Avatar/>', () => {
        const props  = createTestProps()
        const wrapper = createWrapper(props);
        expect(wrapper.find('Avatar').exists()).toEqual(false);
      })
    });

    describe('when there is a props photos', () => {
      it('should render list of photos', () => {
        const props  = createTestProps({ photos })
        const wrapper = createWrapper(props);
        expect(wrapper.find('img')).toHaveLength(3);
      })
    });
  });
});