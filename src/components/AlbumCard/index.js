import React from 'react';
import PropTypes from 'prop-types';

import {
  Card,
  CardBody,
  CardTitle,
} from 'reactstrap';
import Avatar from '../Avatar';

const AlbumCard = ({
  name,
  onClickPicture,
  title,
  photos,
}) => {
  return (
    <Card className="post-card">
      <CardBody>
        {name && <Avatar name={name} />}
        <div>
          {name && <CardTitle tag="h5">{name}</CardTitle>}
          <CardTitle tag="h3">{title}</CardTitle>
          <div className="album-photos">
            {photos && photos.length > 0 && photos.map(photo => (
              <img
                key={photo.id}
                className="mr-1 mb-1"
                src={photo.thumbnailUrl}
                alt={photo.title}
                onClick={() => onClickPicture(photo)}
              />
            ))}
          </div>
        </div>
      </CardBody>
    </Card>
  );
};

AlbumCard.propTypes = {
  name: PropTypes.string,
  onClickPicture: PropTypes.func,
  photos: PropTypes.array,
  title: PropTypes.string.isRequired,
};

export default AlbumCard;