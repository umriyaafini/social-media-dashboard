import React from 'react';
import { connect } from 'react-redux'
import { hideSnackBar } from '../../action';

import { Alert } from 'reactstrap';

const SnackBar = ({ isShow, type, message, hideSnackBar }) => {
  if (!isShow) return null;

  return (
    <div className="fixed-alert">
      <Alert color={type} toggle={hideSnackBar}>
        {message}
      </Alert>
    </div>
  );
}
const mapStateToProps = (state) => ({
  isShow: state.snackbar.isShowSnackBar,
  type: state.snackbar.type,
  message: state.snackbar.message,
});

export default connect(mapStateToProps, {
  hideSnackBar,
})(SnackBar);
