import React from "react";
import { shallow } from 'enzyme';
import FormPost from './index';

function createTestProps (props) {
  return {
    isOpen: true,
    toggle: jest.fn(),
    onSave: jest.fn(),
    ...props,
  }
}

describe("FormPost component", () => {
  const createWrapper = props => shallow(<FormPost {...props} />);

  describe('snapshots', () => {
    it("matches the snapshot", () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('initial state', () => {
      let props;
      let wrapper;
      beforeEach(() => {
        props = createTestProps();
        wrapper = createWrapper(props);
      });

      it('has default title state', () => {
        expect(wrapper.state('title')).toEqual('');
      });

      it('has default body state', () => {
        expect(wrapper.state('body')).toEqual('');
      });
    });

  });

  describe('interaction', () => {

    describe('Input onChange', () => {
      let props;
      let wrapper;
      beforeEach(() => {
        props = createTestProps();
        wrapper = createWrapper(props);
      });

      it("should update state name", () => {
        const input = wrapper.find('#postTitle');
    
        input.props().onChange({target: {
          name: 'title',
          value: 'Electrical Engineering'
        }});
    
        expect(wrapper.state('title')).toEqual('Electrical Engineering');
      });

      it("should update state body", () => {
        const input = wrapper.find('#postBody');
    
        input.props().onChange({ target: {
          name: 'body',
          value: 'My brain is only a receiver, in the Universe there is ...'
        }});
    
        expect(wrapper.state('body')).toEqual('My brain is only a receiver, in the Universe there is ...');
      });
    });

  });

  describe('lifecycle', () => {

    describe('when component is mounted', () => {

      describe('when there is no initialValue props', () => {
        it('should use the default value', () => {
          const props  = createTestProps();
          const wrapper = createWrapper(props);
          expect(wrapper.find('#postTitle').props().value).toEqual('');
        });
      });

    });
  });
});