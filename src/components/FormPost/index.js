import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

const defaultValue = {
  title: '',
  body: '',
}

class FormPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...defaultValue,
    }
  }

  componentDidMount() {
    if (this.props.initialValue) {
      this.setState({
        ...this.props.initialValue,
      });
    } else {
      this.setState({
        ...defaultValue,
      });
    }
  }
  
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit() {
    const { title, body } = this.state;

    this.props.onSave({ title, body });
  }
  
  render() {
    const { title, body } = this.state;
    const { isOpen, titleModal, toggle } = this.props;

    return (
      <Modal
        isOpen={isOpen}
        toggle={toggle}
      >
        <ModalHeader toggle={toggle}>{titleModal}</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="postTitle">Title</Label>
              <Input
                type="text"
                name="title"
                id="postTitle"
                value={title}
                onChange={e => this.handleChange(e)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="postBody">Body</Label>
              <Input
                type="textarea"
                name="body"
                id="postBody"
                rows="5"
                value={body}
                onChange={e => this.handleChange(e)}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => this.handleSubmit()}>Save</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

FormPost.propTypes = {
  initialValue: PropTypes.object,
  isOpen: PropTypes.bool.isRequired,
  onSave: PropTypes.func.isRequired,
  titleModal: PropTypes.string,
  toggle: PropTypes.func.isRequired,
};

export default FormPost;