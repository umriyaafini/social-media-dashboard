import React from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

const defaultValue = {
  name: '',
  email: '',
  body: '',
}

class FormComment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      body: '',
    }
  }

  componentDidMount() {
    if (this.props.initialValue) {
      this.setState({
        ...this.props.initialValue,
      });
    } else {
      this.setState({
        ...defaultValue,
      });
    }
  }
  
  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit() {
    const { name, email, body } = this.state;

    this.props.onSave({ name, email, body });
  }
  
  render() {
    const { name, email, body } = this.state;
    const { isOpen, titleModal, toggle } = this.props;

    return (
      <Modal
        isOpen={isOpen}
        toggle={toggle}
      >
        <ModalHeader toggle={toggle}>{titleModal}</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="commentName">Name</Label>
              <Input
                type="text"
                name="name"
                id="commentName"
                placeholder="Name"
                value={name}
                onChange={e => this.handleChange(e)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="commentEmail">Email</Label>
              <Input
                type="email"
                name="email"
                id="commentEmail"
                placeholder="your@email.com"
                value={email}
                onChange={e => this.handleChange(e)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="commentBody">Body</Label>
              <Input
                type="textarea"
                name="body"
                id="commentBody"
                placeholder="Type your comment"
                rows="5"
                value={body}
                onChange={e => this.handleChange(e)}
              />
            </FormGroup>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={() => this.handleSubmit()}>Save</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

FormComment.propTypes = {
  initialValue: PropTypes.object,
  onSave: PropTypes.func,
  titleModal: PropTypes.string,
};

export default FormComment;