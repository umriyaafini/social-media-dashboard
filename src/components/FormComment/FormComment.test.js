import React from "react";
import { shallow } from 'enzyme';
import FormComment from './index';

function createTestProps (props) {
  return {
    isOpen: true,
    toggle: jest.fn(),
    onSave: jest.fn(),
    ...props,
  }
}

describe("FormComment component", () => {
  const createWrapper = props => shallow(<FormComment {...props} />);

  describe('snapshots', () => {
    it("matches the snapshot", () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('initial state', () => {
      let props;
      let wrapper;
      beforeEach(() => {
        props = createTestProps();
        wrapper = createWrapper(props);
      });

      it('has default name state', () => {
        expect(wrapper.state('name')).toEqual('');
      });

      it('has default email state', () => {
        expect(wrapper.state('email')).toEqual('');
      });

      it('has default body state', () => {
        expect(wrapper.state('body')).toEqual('');
      });
    });

  });

  describe('interaction', () => {

    describe('Input onChange', () => {
      let props;
      let wrapper;
      beforeEach(() => {
        props = createTestProps();
        wrapper = createWrapper(props);
      });

      it("should update state name", () => {
        const input = wrapper.find('#commentName');
    
        input.props().onChange({target: {
          name: 'name',
          value: 'Nikola Tesla'
        }});
    
        expect(wrapper.state('name')).toEqual('Nikola Tesla');
      });

      it("should update state email", () => {
        const input = wrapper.find('#commentEmail');
    
        input.props().onChange({target: {
          name: 'email',
          value: 'nikola.tesla@gmail.com'
        }});
    
        expect(wrapper.state('email')).toEqual('nikola.tesla@gmail.com');
      });

      it("should update state body", () => {
        const input = wrapper.find('#commentEmail');
    
        input.props().onChange({target: {
          name: 'body',
          value: 'My brain is only a receiver, in the Universe there is ...'
        }});
    
        expect(wrapper.state('body')).toEqual('My brain is only a receiver, in the Universe there is ...');
      });
    });

  });

  describe('lifecycle', () => {

    describe('when component is mounted', () => {

      // describe('when there is an initialValue props', () => {
      //   it('should use the given value', () => {
      //     const props  = createTestProps({ initalValue: { name: 'Nikola Tesla' } });
      //     const wrapper = createWrapper(props );
      //     wrapper.instance().componentDidMount();
      //     expect(wrapper.find('#commentName').props().value).toEqual('Nikola Tesla');
      //   });
      // });

      describe('when there is no initialValue props', () => {
        it('should use the default value', () => {
          const props  = createTestProps();
          const wrapper = createWrapper(props);
          expect(wrapper.find('#commentName').props().value).toEqual('');
        });
      });

    });
  });
});