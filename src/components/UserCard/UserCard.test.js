import React from "react";
import { shallow } from 'enzyme';
import UserCard from './index';

const createTestProps = (props) => ({
  name: 'Nikola Tesla',
  ...props,
});

describe('UserCard component', () => {
  const createWrapper = props => shallow(<UserCard {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

});