import React from 'react';
import PropTypes from 'prop-types';

import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
  Container, Row, Col,
} from 'reactstrap';
import Avatar from '../Avatar';

const UserCard = ({
  name,
  email,
  location,
  company,
  onGoToPost,
  onGoToAlbum,
  onCreatePost,
}) => {
  return (
    <Card className="user-card">
      <CardBody>
        <Avatar name={name} />
        <Container>
          <Row>
            <Col>
              <CardTitle tag="h5">{name}</CardTitle>
              {location && <CardText><span role="img" aria-label="location">📍</span> {location}</CardText>}
              {email && <CardText><span role="img" aria-label="email">💌</span> {email}</CardText>}
              {company && <CardText><span role="img" aria-label="company">💼</span> {company}</CardText>}
            </Col>
          </Row>
          <Row className="mt-3 mb-2">
            {onGoToPost && <Col><Button block onClick={onGoToPost}>See posts</Button></Col>}
            {onGoToAlbum && <Col><Button block onClick={onGoToAlbum}>See albums</Button></Col>}
          </Row>
          <Row>
            <Col>
              {onCreatePost && <Button color="primary" block onClick={onCreatePost} >Create post</Button>}
            </Col>
          </Row>
        </Container>
      </CardBody>
    </Card>
  );
};

UserCard.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string,
  location: PropTypes.string,
  company: PropTypes.string,
  onGoToPost: PropTypes.func,
  onGoToAlbum: PropTypes.func,
  onCreatePost: PropTypes.func,
};

export default UserCard;