import React from 'react';
import PropTypes from 'prop-types';

import {
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
  Button,
  ButtonGroup,
} from 'reactstrap';
import Avatar from '../Avatar';

const CommentCard = ({
  name,
  email,
  body,
  onEdit,
  onDelete,
}) => {
  return (
    <Card className="comment-card bg-light">
      <CardBody>
        <Avatar name={name} />
        <div>
          <CardTitle tag="h6">{name}</CardTitle>
          <CardSubtitle className="text-secondary mb-3">{email}</CardSubtitle>
          <CardText>{body}</CardText>

          {(onEdit || onDelete) && (
            <div className="floating-action">
              <ButtonGroup>
                <Button onClick={onEdit}>Edit</Button>
                <Button onClick={onDelete}>Delete</Button>
              </ButtonGroup>
            </div>
          )}
        </div>
      </CardBody>
    </Card>
  );
};

CommentCard.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
};

export default CommentCard;