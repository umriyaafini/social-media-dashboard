import React from "react";
import { shallow } from 'enzyme';
import CommentCard from './index';

const createTestProps = (props) => ({
  name: 'Nikola Tesla',
  email: 'nikola.tesla@google.com',
  body: 'My brain is only a receiver, in the Universe there is ...',
  ...props,
});

describe('CommentCard component', () => {
  const createWrapper = props => shallow(<CommentCard {...props} />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      const props  = createTestProps();
      const wrapper = createWrapper(props);
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('when there onEdit props', () => {
      it('should button edit', () => {
        const props  = createTestProps({ onEdit: jest.fn() })
        const wrapper = createWrapper(props);
        expect(wrapper.find('Button').exists()).toEqual(true);
      })
    });
  });
});