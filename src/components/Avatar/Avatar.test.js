import React from "react";
import { shallow } from 'enzyme';
import Avatar from './index';


describe('Avatar component', () => {
  const wrapper = shallow(<Avatar name="Nikola Tesla" />);

  describe('snapshots', () => {
    it('matches the snapshot', () => {
      expect(wrapper.getElements()).toMatchSnapshot();
    });
  });

  describe('rendering', () => {

    describe('when given props name', () => {
      it('should render 2 first char at each word', () => {
        expect(wrapper.find('h5').text()).toHaveLength(2);
      })
    });

  });
});