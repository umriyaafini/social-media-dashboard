import React from 'react';

const Avatar = ({ name }) => {
  const initial = name && name.split(' ')
    .map(word => word.charAt(0))
    .join('')
    .substring(0, 2)
    .toUpperCase();

  return (
    <div className="avatar">
      <h5>{initial}</h5>
    </div>
  );
}

export default Avatar;
