const API_URL = 'https://jsonplaceholder.typicode.com/';

const callAPI =(
  { path, method, body },
  [ requestType, successType, errorType ],
) => async (dispatch) => {
  dispatch({ type: requestType });

  try {
    const response = await fetch(`${API_URL}${path}`, {
      method,
      body: JSON.stringify(body),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    });
    const json = await response.json();
    dispatch({ type: successType, payload: json });

    if (response.ok) {
      return json;
    }
  } catch (error) {
    dispatch({ type: errorType, payload: error });
    console.error(error);
    return error;
  }
}

export default callAPI;