import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchUserAlbums, fetchUserDetail } from '../../action';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import AlbumLists from '../../components/AlbumLists';

class UserAlbum extends Component {
  componentDidMount() {
    const { userId } = this.props.match.params;
    this.props.fetchUserDetail(userId);
    this.props.fetchUserAlbums(userId);
  }
  
  render() {
    const { albums, userDetail } = this.props;

    return (
      <Container>
        {userDetail && (
          <Row>
            <Col>
              <Breadcrumb>
                <BreadcrumbItem tag="h3">{userDetail.name}'s albums</BreadcrumbItem>
              </Breadcrumb>
            </Col>
          </Row>
        )}
        {albums && albums.length > 0 && (
          <Row>
            <Col>
              <AlbumLists albums={albums} />
            </Col>
          </Row>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  albums: state.user.albums,
  userDetail: state.user.details[ownProps.match.params.userId],
});

export default connect(mapStateToProps, {
  fetchUserAlbums,
  fetchUserDetail,
})(UserAlbum);