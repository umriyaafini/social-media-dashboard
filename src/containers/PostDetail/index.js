import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  addComment,
  editComment,
  deleteComment,
  deletePost,
  editPost,
  fetchComments,
  fetchPostDetail,
  fetchUserDetail,
  showSnackBar,
} from '../../action';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import PostCard from '../../components/PostCard';
import CommentCard from '../../components/CommentCard';
import FormComment from '../../components/FormComment';
import FormPost from '../../components/FormPost';

class PostDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedComment: null,
      isShowModalEditPost: false,
      isShowModalComment: false,
      modalCommentType: 'EDIT', // EDIT, CREATE
    }
  }
  
  componentDidMount() {
    const { postId, userId } = this.props.match.params;
    this.props.fetchUserDetail(userId);
    this.props.fetchPostDetail(postId);
    this.props.fetchComments(postId);
  }

  handleAddComment() {
    this.setState({ selectedComment: null });
    this.setState({ modalCommentType: 'CREATE' });
    this.toggleModalComment();
  }

  handleEditComment(selectedComment) {
    this.setState({ selectedComment });
    this.setState({ modalCommentType: 'EDIT' });
    this.toggleModalComment();
  }

  async handleSaveComment(formData) {
    const { postId } = this.props.match.params;

    if (!this.state.selectedComment) {
      const res = await this.props.addComment(postId, formData);

      if (res && res.id) {
        this.props.showSnackBar({
          message: `Add new comment from ${formData.name}`
        });
      }
    } else {
      const res = await this.props.editComment(
        this.state.selectedComment.id,
        formData,
      );

      if (res && res.id) {
        this.props.showSnackBar({
          message: `Update comment from ${formData.name}`
        });
      }
    }
    this.toggleModalComment();
  }

  async handleDeleteComment(commentId) {
    const res = await this.props.deleteComment(commentId);

    if (res) {
      this.props.showSnackBar({ message: 'Delete a comment'});
    }
  }

  async handleDeletePost() {
    const res = await this.props.deletePost();

    if (res) {
      this.props.showSnackBar({ message: 'Delete a post'});
      // redirect to post list
      this.props.history.push('/post');
    }
  }

  async handleSavePost(formData) {
    const { postId } = this.props.match.params;
    const res = await this.props.editPost(postId, formData);

    if (res && res.id) {
      this.props.showSnackBar({ message: 'Post updated!'});
      this.toggleModalEditPost();
    }
  }

  toggleModalEditPost() {
    this.setState(prevState => ({
      isShowModalEditPost: !prevState.isShowModalEditPost,
    }));
  }

  toggleModalComment() {
    this.setState(prevState => ({
      isShowModalComment: !prevState.isShowModalComment,
    }));
  }
  
  render() {
    const { userDetail, postDetail, comments } = this.props;
    const {
      modalCommentType,
      selectedComment,
      isShowModalEditPost,
      isShowModalComment,
    } = this.state;

    return (
      <Container>
        {userDetail && (
          <Row>
            <Col>
              <Breadcrumb>
                <BreadcrumbItem tag="h3">{userDetail.name}'s posts</BreadcrumbItem>
              </Breadcrumb>
            </Col>
          </Row>
        )}
        {userDetail
        && postDetail
        && (
          <PostCard
            name={userDetail.name}
            title={postDetail.title}
            body={postDetail.body}
            onAddComment={() => this.handleAddComment()}
            onEdit={() => this.toggleModalEditPost()}
            onDelete={() => this.handleDeletePost()}
          />
        )}
        <h4>Comments</h4>
        {comments && comments.length > 0 && comments.map(comment => (
          <CommentCard
            key={comment.id}
            name={comment.name}
            email={comment.email}
            body={comment.body}
            onDelete={() => this.handleDeleteComment(comment.id)}
            onEdit={() => this.handleEditComment(comment)}
          />
        ))}
        {isShowModalEditPost && (
          <FormPost
            initialValue={postDetail}
            isOpen={isShowModalEditPost}
            onSave={formData => this.handleSavePost(formData)}
            titleModal="Edit Post"
            toggle={() => this.toggleModalEditPost()}
          />
        )}
        {isShowModalComment && (
          <FormComment
            initialValue={selectedComment}
            isOpen={isShowModalComment}
            onSave={formData => this.handleSaveComment(formData)}
            titleModal={modalCommentType === 'EDIT' ? 'Edit Comment' : 'New Comment'}
            toggle={() => this.toggleModalComment()}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  comments: state.comment.all || [],
  postDetail: state.post.details[ownProps.match.params.postId],
  userDetail: state.user.details[ownProps.match.params.userId],
});

export default connect(mapStateToProps, {
  addComment,
  deleteComment,
  deletePost,
  editComment,
  editPost,
  fetchComments,
  fetchPostDetail,
  fetchUserDetail,
  showSnackBar,
})(PostDetail);
