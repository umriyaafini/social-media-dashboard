import React, { Component } from 'react';
import { connect } from 'react-redux'
import {
  fetchAlbumDetail,
  fetchAlbumPhotos,
  fetchUserDetail,
} from '../../action';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import AlbumCard from '../../components/AlbumCard';
import PhotoDetail from '../../components/PhotoDetail';

class AlbumDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowModalPhoto: false,
      selectedPhoto: null,
    }
  }
  
  componentDidMount() {
    const { albumId, userId } = this.props.match.params;
    this.props.fetchUserDetail(userId);
    this.props.fetchAlbumDetail(albumId);
    this.props.fetchAlbumPhotos(albumId);
  }

  handleClickPhoto(selectedPhoto) {
    console.log(selectedPhoto, 'ahh');
    this.setState({ selectedPhoto });
    this.togglePhotoDetail();
  }

  togglePhotoDetail() {
    this.setState(prevState => ({
      isShowModalPhoto: !prevState.isShowModalPhoto,
    }));
  }
  
  render() {
    const { albumDetail, photos, userDetail } = this.props;
    const { isShowModalPhoto, selectedPhoto } = this.state;

    return (
      <Container>
        {userDetail && (
          <Row>
            <Col>
              <Breadcrumb>
                <BreadcrumbItem tag="h3">{userDetail.name}'s albums</BreadcrumbItem>
              </Breadcrumb>
            </Col>
          </Row>
        )}
        {userDetail && albumDetail && photos && (
          <Row>
            <Col>
              <AlbumCard
                photos={photos}
                title={albumDetail.title}
                name={userDetail.name}
                onClickPicture={picture => this.handleClickPhoto(picture)}
              />
            </Col>
          </Row>
        )}
        {isShowModalPhoto
        && selectedPhoto
        && (
          <PhotoDetail isOpen toggle={() => this.togglePhotoDetail()} {...selectedPhoto} />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  albumDetail: state.album.details[ownProps.match.params.albumId],
  photos: state.album.photos || [],
  userDetail: state.user.details[ownProps.match.params.userId],
});

export default connect(mapStateToProps, {
  fetchAlbumDetail,
  fetchAlbumPhotos,
  fetchUserDetail,
})(AlbumDetail);
