import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchPosts,  } from '../../action';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import PostCard from '../../components/PostCard';

class Post extends Component {
  componentDidMount() {
    this.props.fetchPosts();
  }

  handleGoToDetail(postId, userId) {
    this.props.history.push(`/post/${userId}/${postId}`);
  }
  
  render() {
    const { posts } = this.props;

    return (
      <Container>
        <Row>
          <Col>
            <Breadcrumb>
              <BreadcrumbItem tag="h3">Posts</BreadcrumbItem>
            </Breadcrumb>
          </Col>
        </Row>
        {posts && posts.length > 0 && posts.map(post => (
          <Row key={post.id}>
            <Col>
              <PostCard
                id={post.id}
                title={post.title}
                body={post.body}
                onGoToDetail={() => this.handleGoToDetail(post.id, post.userId)}
              />
            </Col>
          </Row>
        ))}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.post.all || [],
});

export default connect(mapStateToProps, {
  fetchPosts,
})(Post);
