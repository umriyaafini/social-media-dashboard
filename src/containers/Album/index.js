import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchAlbums } from '../../action/album';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import AlbumLists from '../../components/AlbumLists';

class Album extends Component {
  componentDidMount() {
    this.props.fetchAlbums();
  }
  
  render() {
    const { albums } = this.props;

    return (
      <Container>
        <Row>
          <Col>
            <Breadcrumb>
              <BreadcrumbItem tag="h3">Albums</BreadcrumbItem>
            </Breadcrumb>
          </Col>
        </Row>
        {albums && albums.length > 0 && (
          <Row>
            <Col>
              <AlbumLists albums={albums} />
            </Col>
          </Row>
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  albums: state.album.all || [],
});

export default connect(mapStateToProps, {
  fetchAlbums
})(Album);