import React, { Component } from 'react';
import { connect } from 'react-redux'
import { addPost, fetchUsers, showSnackBar } from '../../action';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import UserCard from '../../components/UserCard';
import FormPost from '../../components/FormPost';

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModalCreatePost: false,
      selectedUser: null,
    }
  }
  
  componentDidMount() {
      this.props.fetchUsers();
  }

  handleGoToPost(userId) {
    this.props.history.push(`/user/${userId}/post`);
  }

  handleGoToAlbum(userId) {
    this.props.history.push(`/user/${userId}/album`);
  }

  hanldeCreatePost(selectedUser) {
    this.setState({ selectedUser });
    this.toggleModalCreatePost();
  }
  
  async handleSave(formData) {
    const userId = this.state.selectedUser.id;
    const data = {
      ...formData,
      userId,
    };

    const res = await this.props.addPost(data);

    if (res && res.id) {
      // close modal
      this.toggleModalCreatePost();
      // show alert
      this.props.showSnackBar({
        message: `Success created new post from ${this.state.selectedUser.name}`
      });
      // redirect to post list page
      this.props.history.push(`/user/${userId}/post`);
    }
  }

  toggleModalCreatePost() {
    this.setState(prevState => ({
      showModalCreatePost: !prevState.showModalCreatePost
    }));
  }
  
  render() {
    const { users } = this.props;
    const { showModalCreatePost } = this.state;

    return (
      <Container>
        <Row>
          <Col>
            <Breadcrumb>
              <BreadcrumbItem tag="h3">User</BreadcrumbItem>
            </Breadcrumb>
          </Col>
        </Row>
        {users
        && users.length > 0
        && (
          <Row>
            {users.map(user => (
              <Col key={user.id} lg="6">
                <UserCard
                  id={user.id}
                  name={user.name}
                  location={user.address.city}
                  email={user.email}
                  company={user.company.name}
                  onGoToPost={() => this.handleGoToPost(user.id)}
                  onGoToAlbum={() => this.handleGoToAlbum(user.id)}
                  onCreatePost={() => this.hanldeCreatePost(user)}
                />
              </Col>
            ))}
          </Row>
        )}
        {showModalCreatePost && (
          <FormPost
            isOpen={showModalCreatePost}
            onSave={formData => this.handleSave(formData)}
            titleModal="New Post"
            toggle={() => this.toggleModalCreatePost()}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  users: state.user.all || [],
});

export default connect(mapStateToProps, {
  addPost,
  fetchUsers,
  showSnackBar,
})(Users);