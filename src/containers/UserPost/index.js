import React, { Component } from 'react';
import { connect } from 'react-redux'
import { fetchUserPosts, fetchUserDetail } from '../../action/user';

import {
  Breadcrumb,
  BreadcrumbItem,
  Container,
  Col,
  Row,
} from 'reactstrap';
import PostCard from '../../components/PostCard';

class UserPost extends Component {
  componentDidMount() {
    const { userId } = this.props.match.params;
    this.props.fetchUserPosts(userId);
    this.props.fetchUserDetail(userId);
  }

  handleGoToDetail(postId, userId) {
    this.props.history.push(`/post/${userId}/${postId}`);
  }
  
  render() {
    const { userDetail, posts } = this.props;

    return (
      <Container>
        {userDetail && (
          <Row>
            <Col>
              <Breadcrumb>
                <BreadcrumbItem tag="h3">{userDetail.name}'s posts</BreadcrumbItem>
              </Breadcrumb>
            </Col>
          </Row>
        )}
        {userDetail
        && posts
        && posts.length > 0
        && posts.map(post => (
          <Row key={post.id}>
            <Col>
              <PostCard
                id={post.id}
                name={userDetail.name}
                title={post.title}
                body={post.body}
                onGoToDetail={() => this.handleGoToDetail(post.id, post.userId)}
              />
            </Col>
          </Row>
        ))}
      </Container>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  posts: state.user.posts || [],
  userDetail: state.user.details[ownProps.match.params.userId],
});

export default connect(mapStateToProps, {
  fetchUserPosts,
  fetchUserDetail,
})(UserPost);
