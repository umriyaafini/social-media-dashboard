import * as ActionType from '../action/actionType';
import callAPI from '../utils/callAPI';

export const fetchPosts = () => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `posts`,
  };

  const types = [
    ActionType.FETCH_ALL_POST_REQUEST,
    ActionType.FETCH_ALL_POST_SUCCESS,
    ActionType.FETCH_ALL_POST_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchPostDetail = (postId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `posts/${postId}`,
  };

  const types = [
    ActionType.FETCH_POST_DETAIL_REQUEST,
    ActionType.FETCH_POST_DETAIL_SUCCESS,
    ActionType.FETCH_POST_DETAIL_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const addPost = (body) => (dispatch) => {
  const dataRequest = {
    method: 'POST',
    path: 'posts',
    body,
  };

  const types = [
    ActionType.ADD_NEW_POST_REQUEST,
    ActionType.ADD_NEW_POST_SUCCESS,
    ActionType.ADD_NEW_POST_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const editPost = (postId, body) => (dispatch) => {
  const dataRequest = {
    method: 'PATCH',
    path: `posts/${postId}`,
    body,
  };

  const types = [
    ActionType.EDIT_POST_REQUEST,
    ActionType.EDIT_POST_SUCCESS,
    ActionType.EDIT_POST_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const deletePost = () => (dispatch) => {
  const dataRequest = {
    method: 'DELETE',
    path: 'posts'
  };

  const types = [
    ActionType.DELETE_POST_REQUEST,
    ActionType.DELETE_POST_SUCCESS,
    ActionType.DELETE_POST_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchComments = (postId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `comments?postId=${postId}`,
  };

  const types = [
    ActionType.FETCH_ALL_COMMENT_REQUEST,
    ActionType.FETCH_ALL_COMMENT_SUCCESS,
    ActionType.FETCH_ALL_COMMENT_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const addComment = (postId, body) => (dispatch) => {
  const dataRequest = {
    method: 'POST',
    path: `comments?postId=${postId}`,
    body,
  };

  const types = [
    ActionType.ADD_NEW_COMMENT_REQUEST,
    ActionType.ADD_NEW_COMMENT_SUCCESS,
    ActionType.ADD_NEW_COMMENT_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const editComment = (commentId, body) => (dispatch) => {
  const dataRequest = {
    method: 'PATCH',
    path: `comments/${commentId}`,
    body,
  };

  const types = [
    ActionType.EDIT_POST_REQUEST,
    ActionType.EDIT_POST_SUCCESS,
    ActionType.EDIT_POST_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const deleteComment = (commentId) => (dispatch) => {
  const dataRequest = {
    method: 'DELETE',
    path: `comments/${commentId}`
  };

  const types = [
    ActionType.DELETE_COMMENT_REQUEST,
    ActionType.DELETE_COMMENT_SUCCESS,
    ActionType.DELETE_COMMENT_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};