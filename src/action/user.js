import * as ActionType from '../action/actionType';
import callAPI from '../utils/callAPI';

export const fetchUsers = () => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: 'users',
  };

  const types = [
    ActionType.FETCH_ALL_USER_REQUEST,
    ActionType.FETCH_ALL_USER_SUCCESS,
    ActionType.FETCH_ALL_USER_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchUserDetail = (userId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `users/${userId}`,
  };

  const types = [
    ActionType.FETCH_USER_DETAIL_REQUEST,
    ActionType.FETCH_USER_DETAIL_SUCCESS,
    ActionType.FETCH_USER_DETAIL_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchUserPosts = (userId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `posts?userId=${userId}`,
  };

  const types = [
    ActionType.FETCH_USER_POSTS_REQUEST,
    ActionType.FETCH_USER_POSTS_SUCCESS,
    ActionType.FETCH_USER_POSTS_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchUserAlbums = (userId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `albums?userId=${userId}`,
  };

  const types = [
    ActionType.FETCH_USER_ALBUMS_REQUEST,
    ActionType.FETCH_USER_ALBUMS_SUCCESS,
    ActionType.FETCH_USER_ALBUMS_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};