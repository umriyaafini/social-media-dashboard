import * as ActionType from '../action/actionType';
import callAPI from '../utils/callAPI';

export const fetchAlbums = () => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: 'albums',
  };

  const types = [
    ActionType.FETCH_ALL_ALBUM_REQUEST,
    ActionType.FETCH_ALL_ALBUM_SUCCESS,
    ActionType.FETCH_ALL_ALBUM_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchAlbumDetail = (albumId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `albums/${albumId}`,
  };

  const types = [
    ActionType.FETCH_ALBUM_DETAIL_REQUEST,
    ActionType.FETCH_ALBUM_DETAIL_SUCCESS,
    ActionType.FETCH_ALBUM_DETAIL_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};

export const fetchAlbumPhotos = (albumId) => (dispatch) => {
  const dataRequest = {
    method: 'GET',
    path: `photos?albumId=${albumId}`,
  };

  const types = [
    ActionType.FETCH_ALL_PHOTO_REQUEST,
    ActionType.FETCH_ALL_PHOTO_SUCCESS,
    ActionType.FETCH_ALL_PHOTO_FAILURE,
  ];

  return dispatch(callAPI(dataRequest, types));
};