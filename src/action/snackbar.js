import * as ActionType from '../action/actionType';

export const showSnackBar = ({ message, type }) => (dispatch) => {
  setTimeout(() => {
    dispatch({
      type: ActionType.HIDE_SNACKBAR,
    });
  }, 5000);

  return dispatch({
    type: ActionType.SHOW_SNACKBAR,
    payload: {
      message,
      type,
    },
  });
};

export const hideSnackBar = () => dispatch => dispatch({
  type: ActionType.HIDE_SNACKBAR,
});
