export const SHOW_SNACKBAR = 'SHOW_SNACKBAR';
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR';

export const SET_ACTIVE_SIDEBAR = 'SET_ACTIVE_SIDEBAR';
export const RESET_ACTIVE_SIDEBAR = 'RESET_ACTIVE_SIDEBAR';

// USER
export const FETCH_ALL_USER_REQUEST = 'FETCH_ALL_USER_REQUEST';
export const FETCH_ALL_USER_SUCCESS = 'FETCH_ALL_USER_SUCCESS';
export const FETCH_ALL_USER_FAILURE = 'FETCH_ALL_USER_FAILURE';

export const FETCH_USER_DETAIL_REQUEST = 'FETCH_USER_DETAIL_REQUEST';
export const FETCH_USER_DETAIL_SUCCESS = 'FETCH_USER_DETAIL_SUCCESS';
export const FETCH_USER_DETAIL_FAILURE = 'FETCH_USER_DETAIL_FAILURE';

export const FETCH_USER_POSTS_REQUEST = 'FETCH_USER_POSTS_REQUEST';
export const FETCH_USER_POSTS_SUCCESS = 'FETCH_USER_POSTS_SUCCESS';
export const FETCH_USER_POSTS_FAILURE = 'FETCH_USER_POSTS_FAILURE';

export const FETCH_USER_ALBUMS_REQUEST = 'FETCH_USER_POSTS_REQUEST';
export const FETCH_USER_ALBUMS_SUCCESS = 'FETCH_USER_ALBUMS_SUCCESS';
export const FETCH_USER_ALBUMS_FAILURE = 'FETCH_USER_ALBUMS_FAILURE';

// POST
export const FETCH_ALL_POST_REQUEST = 'FETCH_ALL_POST_REQUEST';
export const FETCH_ALL_POST_SUCCESS = 'FETCH_ALL_POST_SUCCESS';
export const FETCH_ALL_POST_FAILURE = 'FETCH_ALL_POST_FAILURE';

export const FETCH_POST_DETAIL_REQUEST = 'FETCH_POST_DETAIL_REQUEST';
export const FETCH_POST_DETAIL_SUCCESS = 'FETCH_POST_DETAIL_SUCCESS';
export const FETCH_POST_DETAIL_FAILURE = 'FETCH_POST_DETAIL_FAILURE';

export const ADD_NEW_POST_REQUEST = 'ADD_NEW_POST_REQUEST';
export const ADD_NEW_POST_SUCCESS = 'ADD_NEW_POST_SUCCESS';
export const ADD_NEW_POST_FAILURE = 'ADD_NEW_POST_FAILURE';

export const EDIT_POST_REQUEST = 'EDIT_POST_REQUEST';
export const EDIT_POST_SUCCESS = 'EDIT_POST_SUCCESS';
export const EDIT_POST_FAILURE = 'EDIT_POST_FAILURE';

export const DELETE_POST_REQUEST = 'DELETE_POST_REQUEST';
export const DELETE_POST_SUCCESS = 'DELETE_POST_SUCCESS';
export const DELETE_POST_FAILURE = 'DELETE_POST_FAILURE';

// POST COMMENT
export const FETCH_ALL_COMMENT_REQUEST = 'FETCH_ALL_COMMENT_REQUEST';
export const FETCH_ALL_COMMENT_SUCCESS = 'FETCH_ALL_COMMENT_SUCCESS';
export const FETCH_ALL_COMMENT_FAILURE = 'FETCH_ALL_COMMENT_FAILURE';

export const ADD_NEW_COMMENT_REQUEST = 'ADD_NEW_COMMENT_REQUEST';
export const ADD_NEW_COMMENT_SUCCESS = 'ADD_NEW_COMMENT_SUCCESS';
export const ADD_NEW_COMMENT_FAILURE = 'ADD_NEW_COMMENT_FAILURE';

export const EDIT_COMMENT_REQUEST = 'EDIT_COMMENT_REQUEST';
export const EDIT_COMMENT_SUCCESS = 'EDIT_COMMENT_SUCCESS';
export const EDIT_COMMENT_FAILURE = 'EDIT_COMMENT_FAILURE';

export const DELETE_COMMENT_REQUEST = 'DELETE_COMMENT_REQUEST';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';

// ALBUM
export const FETCH_ALL_ALBUM_REQUEST = 'FETCH_ALL_ALBUM_REQUEST';
export const FETCH_ALL_ALBUM_SUCCESS = 'FETCH_ALL_ALBUM_SUCCESS';
export const FETCH_ALL_ALBUM_FAILURE = 'FETCH_ALL_ALBUM_FAILURE';

export const FETCH_ALBUM_DETAIL_REQUEST = 'FETCH_ALBUM_DETAIL_REQUEST';
export const FETCH_ALBUM_DETAIL_SUCCESS = 'FETCH_ALBUM_DETAIL_SUCCESS';
export const FETCH_ALBUM_DETAIL_FAILURE = 'FETCH_ALBUM_DETAIL_FAILURE';

// PHOTO
export const FETCH_ALL_PHOTO_REQUEST = 'FETCH_ALL_PHOTO_REQUEST';
export const FETCH_ALL_PHOTO_SUCCESS = 'FETCH_ALL_PHOTO_SUCCESS';
export const FETCH_ALL_PHOTO_FAILURE = 'FETCH_ALL_PHOTO_FAILURE';

export const FETCH_PHOTO_DETAIL_REQUEST = 'FETCH_PHOTO_DETAIL_REQUEST';
export const FETCH_PHOTO_DETAIL_SUCCESS = 'FETCH_PHOTO_DETAIL_SUCCESS';
export const FETCH_PHOTO_DETAIL_FAILURE = 'FETCH_PHOTO_DETAIL_FAILURE';